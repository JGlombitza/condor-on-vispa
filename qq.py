#!/usr/bin/env python
import time
import subprocess
import sys
import os

length = 89
refresh_period = 11.
FPS = 2.


def startProgress():
    global progress_x
    sys.stdout.write("[" + "-" * (length - 2) + "]" + chr(8) * (length - 1))
    sys.stdout.flush()
    progress_x = 0


def progress(x):
    global progress_x
    x = int(x * (length - 2))
    sys.stdout.write("#" * (x - progress_x))
    sys.stdout.flush()
    progress_x = x


def endProgress():
    print("#" * (length - 2 - progress_x) + "]\n")
    sys.stdout.flush()


while True:

    rows, length = os.popen('stty size', 'r').read().split()
    length = int(length)
    # process = subprocess.Popen("clear", shell=True)
    process = subprocess.Popen("condor_q -g -allusers -nobatch", shell=True)
    process = subprocess.Popen("condor_q -g -allusers", shell=True)
    time.sleep(0.23)
    print("_" * length)
    print("_" * ((length - 15) // 2) + "| GPU CLUSTER |" + "_" * ((length - 15) // 2) + "_" * ((length + 1) % 2))
    print("ID         OWNER           SUBMITTED    RUN_TIME HOST(S)")
    for ind in range(1, 11):
        # print(" _ "*(length//3))
        process = subprocess.Popen("condor_q -allusers -r -g -nobatch --constraint RequestGPUs!=0 | grep '@vispa-gpu{num:02d}' --color=auto".format(num=ind), shell=True)
        time.sleep(0.1)

    for ind in range(1, 3):
        # print(" _ "*(length//3))
        process = subprocess.Popen("condor_q -allusers -r -g -nobatch --constraint RequestGPUs!=0 | grep '@vispa-worker{num:02d}' --color=auto".format(num=ind), shell=True)
        time.sleep(0.1)

    time.sleep(0.1)
    print("_" * length)
    print("_" * ((length - 15) // 2) + "|   MY JOBS   |" + "_" * ((length - 15) // 2) + "_" * ((length + 1) % 2))
    print("ID         OWNER           SUBMITTED    RUN_TIME   ST PRI SIZE    CMD")
    process = subprocess.Popen("condor_q -allusers -g -nobatch | grep 'JGlombitza' --color='always'", shell=True)
    time.sleep(0.4)
    print("_" * length)
    startProgress()
    for i in range(int(refresh_period * FPS)):
        p = float(i) / (refresh_period * FPS)
        progress(p)
        time.sleep(1. / FPS)

    endProgress()
    time.sleep(1. / FPS)
#    print("update")
    time.sleep(1. / FPS)
    sys.stdout.flush()
