##cookbook
import subprocess
import argparse
import time
import os
import glob
import sys


def RowToList(Row):
    Row = Row.replace("\x08", "")
    liste = Row.splitlines()
    if len(liste) > 0:
        return liste[len(liste)-1]
    else:
        return ""


def SaveToString(line):
    if line != "":
        return line + "\n"
    else:
        return ""


def SaveToBook(page):
    return page


def Reserve(a=""):
    return a + "\n"


def PrintStars(book, columns):
    Buffer = "Stars"
    a = ""
    for i in range(columns):
        a += "+"
    return book.replace(Buffer, a)


def PrintBars(book, columns):
    Buffer = "Bar"
    a = ""
    for i in range(columns):
        a += "_"
    return book.replace(Buffer, a)


def PrintTitle(book, columns):
    Buffer = "Title"
    a = ""
    if columns % 2 == 1:
        title = "| JobScreen |"
    else:
        title = "| Job Screen |"
    for i in range(columns-len(title)):
        a += " "
        if i == (columns-len(title))//2-1:
            a += title
    return book.replace(Buffer, a)


def PrintJobs(book, columns, jobs):
    Buffer = "JobBUFFER"
    title = ""
    border = ""
    for j in range((columns-12)//2):
        border += "_"
    for i in range(jobs):
        if i > 9:
            title = border + "|  Job: " + str(i+1) + "  |" + border
            book = book.replace(Buffer, title, 1)  # 12
        else:
            title = border + "|  Job: " + str(i+1) + "   |" + border
            book = book.replace(Buffer, title, 1)  # 12
    return book


def ClearScreen(Upscale, columns):
    a = ""
    for k in range(columns):
        a += " "
    for l in range(Upscale):
        sys.stdout.write("\033[F")
    for m in range(Upscale):
        print(a)
    for n in range(Upscale):
        sys.stdout.write("\033[F")


def Book_Print(book, columns, jobs):
    swapper = []
    swapString = ""
    replace = " - "
    book = PrintBars(book, columns)
    book = PrintTitle(book, columns)
    book = PrintJobs(book, columns, jobs)
    book = PrintStars(book, columns)
    PrintBook = book.splitlines()  # String -> LIST
    NewBook = []
    loop = len(PrintBook)
    for i in range(loop):
        if len(PrintBook[i]) > columns:
            swapString = ""
            if PrintBook[i].find("Error") >= 0:
                replace = " "
            swapper = PrintBook[i].split(replace)
            for k in range(len(swapper)-1):
                swapper[k+1] = replace + swapper[k+1]
            o = 0
            while True:
                if o == len(swapper):
                    NewBook.append(swapString)
                    swapString = ""
                    break
                elif len(swapString) + len(swapper[o]) < columns:
                    swapString += swapper[o]
                    o += 1
                else:
                    if swapString == "":
                        #                        print("BUG")
                        break
                    NewBook.append(swapString)
                    swapString = ""
        else:
            NewBook.append(PrintBook[i])
    Upscale = len(NewBook)
    for g in range(Upscale):
        print(NewBook[g])
    return Upscale


parser = argparse.ArgumentParser(description='Cookbook for vispa Condor')

parser.add_argument("--v", action="store", dest='verbose', default=True, type=bool)
parser.add_argument("--f", action="store", dest='FileName', type=str)
parser.add_argument("--n", action="store", dest='NumberOfJobs', default=1, type=int)
parser.add_argument("--m", action="store", dest='ModelName', default="", type=str)
parser.add_argument("--c", action="store", dest='NumberOfCPUs', default=1, type=int)
parser.add_argument("--g", action="store", dest='NumberOfGPUs', default=1, type=int)
parser.add_argument("--mem", action="store", dest='MemoryUsage', default=25000, type=int)
parser.add_argument("--gram", action="store", dest='GPUMemory', default=0, type=int)
parser.add_argument("--req", action="store", dest='Requirements', default='machine=="vispa-worker02.physik.rwth-aachen.de"', type=str)
parser.add_argument("--nncp", action="store", dest='NoCopy', default=True, type=bool)
FileNames = []
FileErrors = []


def Command(log_dir, FileName, NoCopy):
    if NoCopy is True:
        JobLocation = FileName
    else:
        JobLocation = log_dir + "/" + FileName

    MemoryUsage = int(parsed.MemoryUsage)
    if parsed.GPUMemory > 0:
        gpu_flag = " -M " + str(parsed.GPUMemory)
    else:
        gpu_flag = " -g " + str(parsed.NumberOfGPUs)
    if parsed.Requirements != parser.get_default("Requirements"):
        require_flag = str(" -r " + parsed.Requirements)
    else:
        require_flag = ""
    condorFlags = gpu_flag + require_flag + " -c " + str(parsed.NumberOfCPUs) + " -L " + log_dir + " -m " + str(MemoryUsage)
    programFlags = " --log_dir " + log_dir
    bashCommand = "submit" + condorFlags + " python " + str(JobLocation) + programFlags
    print("Submit job")
    process = subprocess.Popen(bashCommand.split())
    output, error = process.communicate()


parsed = parser.parse_args()
if parsed.ModelName == "":
    ModelName = parsed.FileName
    ModelName.replace(" ", "").rstrip(ModelName[-3:]).upper()
else:
    ModelName = parsed.ModelName

print(parsed)
runNumber = 0
time.sleep(1)
log_dir = []
for i in range(parsed.NumberOfJobs):
    log_dir.append(str('/home/JGlombitza/scratch/Airshower/Ergebnisse/' + ModelName + 'Run_' + str(runNumber)))
    while os.path.exists(str(log_dir[i])):
        runNumber += 1
        log_dir[i] = str('/home/JGlombitza/DataLink/Ergebnisse/' + ModelName + 'Run_' + str(runNumber))
    print("Creating Folder", log_dir[i])
    output = subprocess.check_output("mkdir " + log_dir[i], shell=True)
    time.sleep(0.6)  # 1
    copy = subprocess.Popen(str("cp " + str(parsed.FileName) + " " + str(log_dir[i]) + "/" + str(parsed.FileName)), shell=True)
    Command(log_dir=log_dir[i], FileName=parsed.FileName, NoCopy=parsed.NoCopy)
    output_file = glob.glob(log_dir[i]+"/*output.txt")
    error_file = glob.glob(log_dir[i]+"/*error.txt")
    FileNames.append(output_file)
    FileErrors.append(error_file)
    copy = subprocess.Popen(str("cp " + str(parsed.FileName) + " " + str(log_dir[i]) + "/" + str(parsed.FileName)), shell=True)


# ##################### START DISPLAY ######################

if parsed.verbose is True:
    Upscale = 0
    while True:
        time.sleep(5)
        loop = len(FileNames)
        forforforlast = ""
        forforlast = ""
        forlast = ""
        last = ""
        Errorlast = ""
        page = ""
        book = ""
        book += Reserve("Stars")
        book += Reserve("Title")
        book += Reserve("Stars")
        book += str(ModelName) + "\n"
        for i in range(loop):

            if i != 0:
                book += Reserve("Bar",)
            book += Reserve("JobBUFFER")
            page = ""
            try:
                f = open(''.join(FileNames[i]))
                for line in (line for line in f if line.rstrip('\n')):
                    forforforlast = forforlast
                    forforlast = forlast
                    forlast = last
                    last = line

                f = open(''.join(FileErrors[i]))
                for line in (line for line in f if line.rstrip('\n')):
                    Errorlast = line
                if Errorlast.find("Error") >= 0:
                    page += SaveToString(RowToList(Errorlast))
                elif SaveToString(RowToList(last)) == "File begin\n":
                    page += SaveToString("     Start job")
                elif SaveToString(RowToList(last)) == "Finished!\n":
                    page += "Job finished! Check:\n"
                    if FileNames[i] > columns:
                        page += ModelName+"Run_" + str(runNumber-loop+i+1) + "\n"
                    else:
                        page += ''.join(FileNames[i]) + "\n"
                else:
                    page += SaveToString("     Job Running... without output\n")

                if forlast != "" and last != "Finished!\n":
                    page += SaveToString(RowToList(forforforlast))
                    page += SaveToString(RowToList(forforlast))
                    page += SaveToString(RowToList(forlast))
                    page += SaveToString(RowToList(last))
            except FileNotFoundError:
                page = SaveToString("     Job Standing in Queue\n")
                output_file = glob.glob(log_dir[i]+"/*output.txt")
                error_file = glob.glob(log_dir[i]+"/*error.txt")
                FileNames[i] = output_file
                FileErrors[i] = error_file

            book += SaveToBook(page)

        book += Reserve("Stars")
        columns = int(os.popen('stty size', 'r').read().split()[1])
        ClearScreen(Upscale, columns)
        Upscale = Book_Print(book, columns, jobs=loop)
#def StatusOfJob(page):
#

##def CenterWord(Str="", columns):
##    if columns%2 == 1:
##        title ="| JobScreen |"
##    else:
##        title = "| Job Screen |"
##    for i in range(columns-len(title)-4):
##        a += "_"
##        if i == (columns-10)/2
##            a += title
##    return book.replace(Title, a=

#def StatusOfFile(FileNames):
### Checking For Standing in Queue, Errors
#    return 0
#    #Error =  open(''.join(FileNames) + "error.txt")
#
##def restart():
##    process = subprocess.Popen("clear", shell=True)
##    PrintHeader()
##

##    if columns =! os.popen('stty size', 'r').read().split()[1]:

##        restart()
